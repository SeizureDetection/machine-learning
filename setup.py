from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Analysis for features of seizures in calcium data and evaluation of algorithms to detect seizures.',
    author='Dominik Thomas',
    license='',
)
