# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from sklearn import tree
from sklearn import ensemble
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import svm

from scipy.signal import spectrogram
from scipy.stats import entropy
import matplotlib.pyplot as plt
import graphviz 


# load data

validators = pd.read_csv("../../Data/Validators/2016-10-13T1513.dat",
                   sep="\t", header=None, names=['start', 'end']);
                         
raw = pd.read_csv("../../Data/Raw/2016-10-13T1513.dat",
                   sep="\t", header=None, names=['yvalue']);
                  
start_second = 275
end_second = 1810
freq = 2000 # abtastrate
start_value = start_second*freq
end_value = end_second*freq
window = 1000 # half second window for classification
# skip everything before start

# prepare data

raw = raw[start_value:end_value] 

raw['group'] = raw.index // window
validators['start'] = validators['start'] // window 
validators['end'] = validators['end'] // window 

# todo chech if some start or end points are both in half a second
validators['yvalue'] = 0.0

group = raw.groupby('group')
#data_mean =  group.mean()
data_var = group.var()*10000
#data_linelength = None
# todo manhattan length
data_max = group.max()
data_entropy = data_var.apply(entropy).loc[:][0]

#max min entfernt, da nicht signifikant

data = pd.concat([data_var, data_max], axis=1)
data = data.copy()
data.columns = ['var', 'max']
data['group'] = data.index
#todo seizure_before and entropy




data['seizure'] = 0

for index, row in validators.iterrows():
    start = row['start'].astype(np.int64)
    end = row['end'].astype(np.int64)
    #todo check if
    data.seizure.loc[(data['group'] >= start) & (data['group'] <= end)] = 1
    #for i in range(start, end): 
    #    if (i > len(data.index)):
    #        break
    #    data.seizure.iloc[i] = 1
        



#starts = validators.start // window
#ends = validators.end // window
#data.plot(y='var')

data['seiz_bef'] = data.seizure.shift(fill_value=0)
data['change'] = data['seizure'].diff()


# 0 for normal, 1 for seizure start, 2 for seizure, 3 for normal start
data['state'] = data['seizure']
data['state'].loc[data['state'] == 1] = 2
data['state'].loc[data['change'] == 1] = 1
data['state'].loc[data['change'] == -1] = 3

ax = data.plot( y='seiz_bef', x='group', c='g')
ax = data.plot( y='seizure', x='group', ax=ax, c='r')
ax = data.plot( y='change', x='group', ax=ax, c='b')
ax = data.plot(kind='scatter', y='seiz_bef', x='group', ax=ax, c='g')
ax = data.plot(kind='scatter', y='seizure', x='group', ax=ax, c='r')
ax = data.plot(kind='scatter', y='change', x='group', ax=ax, c='b')
ax = data.plot(y='state', x='group', ax=ax, c='black')


# todo scale feauters
#todo corr between features
#todo pca

# plot data
seizure=data[data.seizure==1]
normal=data[data.seizure==0]

# todo sb pairwise plot for features?

raw['tmp'] = raw.index / window

ax = seizure.plot(kind='scatter', x='group', y='var', color='red')
ax = raw.plot(x='tmp', y='yvalue',color='orange', ax=ax)
ax = normal.plot(kind='scatter', x='group', y='var', color='black', ax=ax)
ax = data.plot(x='group', y='var', color='grey', ax=ax)
#colors = np.where(data.seizure == 1, 'red', 'black')
#ax = data.plot(y='var', c=colors)
ax = validators.plot(kind='scatter', x='start', y='yvalue', color='green', ax=ax)
ax = validators.plot(kind='scatter', x='end', y='yvalue', color='purple', ax=ax)
ax = data.plot(y='max', ax=ax, color='black')

ax.get_figure().savefig('generalplot.pdf')



# decision tree without seiz before
features = ['var', 'max']#, 'seiz_bef']
X =  data[features]
Y = data['state']

# todo fit on seizure and aon state

#raw_seizures = 
#todo plot first twenty seizures

X_train, X_test, Y_train, Y_test = train_test_split( X, Y, test_size = 0.3, random_state = 42)

clf1 = tree.DecisionTreeClassifier(min_samples_leaf=10, max_depth=3, random_state=42)
clf1 = clf1.fit(X_train,Y_train)

Y_pred = clf1.predict(X_test)

accuracy = accuracy_score(Y_test,Y_pred)
print('Decision tree without "seiz_bef" acc.:',accuracy)
importances = list(zip(features, clf1.feature_importances_))
print('Feature importance')
print(importances)

dot_data = tree.export_graphviz(clf1, out_file=None,   
                      filled=True, rounded=True,
                      feature_names=features,
                      class_names=['normal', 'seizure_start', 'seizure', 'normal_start'],
                      special_characters=True)  
graph = graphviz.Source(dot_data)  
graph.render('decisiontree_no_seiz_bef')

# decision tree with seiz before
features = [ 'var', 'max', 'seiz_bef']
X =  data[features]
Y = data['state']

X_train, X_test, Y_train, Y_test = train_test_split( X, Y, test_size = 0.3, random_state = 42)

clf2 = tree.DecisionTreeClassifier(min_samples_leaf=5, max_depth=3, random_state=42)
clf2 = clf2.fit(X_train,Y_train)

Y_pred = clf2.predict(X_test)

accuracy = accuracy_score(Y_test,Y_pred)

print('Decision tree with "seiz_bef" acc.:',accuracy)
importances = list(zip(features, clf2.feature_importances_))
print('Feature importance')
print(importances)

dot_data = tree.export_graphviz(clf2, out_file=None,   
                      filled=True, rounded=True,
                      feature_names=features,
                      class_names=['normal', 'seizure_start', 'seizure', 'normal_start'],
                      special_characters=True)  
graph = graphviz.Source(dot_data)  
graph.render('decisiontree_with_seiz_bef')

#random forest


clf3 = ensemble.RandomForestClassifier(max_depth=3, n_estimators=100, min_samples_leaf=5, random_state=42)
clf3 = clf3.fit(X_train,Y_train)

Y_pred = clf3.predict(X_test)

accuracy = accuracy_score(Y_test,Y_pred)

print('RandomForest acc.:',accuracy)
importances = list(zip(features, clf3.feature_importances_))
print('Feature importance')
print(importances)

# svm
clf4 = svm.SVC(gamma='scale')
clf4 = clf4.fit(X_train, Y_train)
Y_pred = clf4.predict(X_test)

accuracy = accuracy_score(Y_test,Y_pred)

print('SVM acc.:',accuracy)

#https://scikit-learn.org/stable/auto_examples/calibration/plot_calibration_curve.html#sphx-glr-auto-examples-calibration-plot-calibration-curve-py

# confusion matrix 
# gridsearch
# todo show wrong seizures
# todo mismatch
# todo for feature plot see https://www.datacamp.com/community/tutorials/random-forests-classifier-python
# todo fft of seizures and of no seizres
#todo dummy classifier
#todo cross validate
#todo hmm
#todo extratrees

# todo extratreeclassfierer

"""
clf2 = ensemble.RandomForestClassifier(max_depth=4, n_estimators=10, max_features=3)
clf2 = clf2.fit(X,Y)

dot_data = tree.export_graphviz(clf2, out_file=None,   
                      filled=True, rounded=True,
                      feature_names=features,
                      class_names=['normal', 'seizure'],
                      special_characters=True)  
graph = graphviz.Source(dot_data)  
graph.render('randomforest')




#prepare data
window_start = start_value
j = 0
lst = []
sum_ = 0
is_seizure = False
for i in range(start_value, len(raw.index)):
    j = j+1
    sum_ = sum_ + raw.iloc[i]
    
    
    if(j == window):
        mean = sum_/window
        
        lst.append([window_start, mean])
        sum_ = j = 0
        window_start = i
        isSeizure = False
        # todo save in dataframe

data = pd.DataFrame(lst, columns = ['start', 'mean'])
"""