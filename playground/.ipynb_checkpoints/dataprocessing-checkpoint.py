# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 10:58:12 2019

@author: thomasdo
"""
import glob
import pandas as pd
import os
from scipy import signal

# use ../src/data/make_dataset.py

#../../../Data/raw/calcium/forGAERSproject/*.dat
"""
def apply50HzFilter(files):
    sos = signal.butter(4, 50, fs=2000, output='sos')    
    for path in files:
        print(path)
        file = os.path.basename(path)
        df = pd.read_csv(path,
                       sep="\t", header=None, names=['yvalue']);
        print("Transform", file)
        df["filtered"] = signal.sosfilt(sos, df.yvalue)
        filepath_save = "C:/Users/thomasdo/Projects/Data/interim/filtered/" + file
        df.to_csv(filepath_save, columns=["filtered"], header=False, index=False)
    

def csvToFeather(files):
    for file in files:
        filename = os.path.splitext(file)[0]
        print('Transform ', filename, ' from .csv to .feather')
        df = pd.read_csv(file, names=['yvalue'])
        df.to_feather(filename + '.feather')


raw_files = glob.glob("C:/Users/thomasdo/Projects/Data/raw/calcium/forGAERSproject/*.dat")
#raw_files = glob.glob("C:/Users/thomasdo/Projects/Data/*.dat")
apply50HzFilter(raw_files)

interimCSVFiles = glob.glob("C:/Users/thomasdo/Projects/Data/interim/filtered/*.dat")
#interimCSVFiles = ['C:/Users/thomasdo/Projects/Data/interim/filtered/2019-03-06T1330.dat',
#                   'C:/Users/thomasdo/Projects/Data/interim/filtered/2019-03-07T1714.dat']
csvToFeather(interimCSVFiles)
"""