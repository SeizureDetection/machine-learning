# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 13:03:52 2019

@author: thomasdo
"""
#TODO https://towardsdatascience.com/manage-your-data-science-project-structure-in-early-stage-95f91d4d0600

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from scipy import signal

from src.data.prepare import load_feather, load_csv, stripRawData, resampleToMean, convertIntIndexToTimeDelta
import src.data.seizures

def saveFigure(figure, filename, dirname='', nodatetime=False):  
    timestamp = ''
    if not nodatetime:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H%M%S")+'_'

    figure.savefig(dirname+timestamp+filename+'.pdf')
    figure.savefig(dirname+timestamp+filename+'.png')

# TODO move to sns
# TODO doc
# TODO move to visulaize
#TODO forward fig arguments as kwargs
def plotPeaks(peaks_pos, df, y, threshold, title='', filename='', **kwargs):#dirname='', nodatetime=False):
    """*kwargs: forward to saveFigure"""
    fig, ax = plt.subplots()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Variance peaks')
    ax.set_ylabel('calcium level')
    ax = df.plot(y='yvalue', c='r', ax=ax, label='raw data' )
    ax2 = ax.twinx()
    ax2.set_ylabel('variance value')
    df.plot(y=y, c='orange', ax=ax2, label='variance')
  #  ax2.axhline(y=min_height)
    ax2.axhline(y=threshold, label='Threshold')
    df.iloc[peaks_pos].plot(style='.', y=y, c='blue', ax=ax2, label='peaks')
    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax.legend(lines + lines2, labels + labels2, loc=0)
    ax2.get_legend().remove()
    if filename:
        saveFigure(fig, filename, **kwargs)
    #todo ttile=
    return fig



def plotPeakProminencesHist(prominences, title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peak pomincences distribution')
    ax = plt.hist(prominences, 50)
    plt.xlabel('variance prominence')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax

#todo rename to plot PeaksHist and merg with other plots?
def plotPossibleSeizurePeaksHist(pos_seizure, title='', filename='', **kwargs):
    fig, ax = plt.subplots()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Distribution of possible seizure peaks')
    ax = pos_seizure.plot(kind='hist', y='var', bins=50, ax=ax)
    ax.get_legend().remove()   
    plt.ylabel('frequency')
    plt.xlabel('variance')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax
    


# TODO change xlabel to time
def plotPeakProperties(peaks, y, prominences, results_half, threshold,  title='', filename='', **kwargs):
    contour_heights = y[peaks] - prominences
    fig = plt.figure()
    plt.plot(y.values)
    plt.plot(peaks, y[peaks], "x")
    plt.vlines(x=peaks, ymin=contour_heights, ymax=y[peaks])
    plt.hlines(*results_half[1:], color="C2")
    plt.axhline(y=threshold)
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peaks with properties')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return plt.show() #todo necessary??
    

# TODO width to time or timedelta
# TODO sns plot with fit
def plotPeakWidthHist(width,  title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peak width distribution')
    ax = plt.hist(width, 50)
    plt.xlabel('time')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax
    

# peaks distance
# TODO to time data type, give dataframe
def plotPeakDiff(peaks,  title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Difference between peaks')
    ax = plt.hist(np.diff(peaks), 50, range=(0, 75))
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax
        

def plotSeizureLengthHist(length_df_in_s,  title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Seizure length')
    ax = length_df_in_s.plot.hist(bins=18, range=(1,10))
    plt.xlabel('s')
    plt.ylabel('frequency')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax
    
# TODO rename to plot seizures
# TODO https://matplotlib.org/api/collections_api.html#matplotlib.collections.BrokenBarHCollection.span_where
# https://matplotlib.org/gallery/lines_bars_and_markers/span_regions.html#sphx-glr-gallery-lines-bars-and-markers-span-regions-py
def plotSeizurePeaks(raw_df, start_peaks_df, end_peaks_df,  title='', filename='', **kwargs):
    ax = raw_df.plot(x='time', y='var', c='orange')
    #ax = cleaned_peaks.plot(x='s',y='var', style='.', ax=ax)
    fig = ax.get_figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Seizures')

    ax = start_peaks_df.plot(y='var', style='x', ax=ax, c='r', label='start')
    ax = end_peaks_df.plot(y='var', style='x', ax=ax, c='g', label='end')
    if filename:
        saveFigure(fig, filename, **kwargs)
    return ax
    

#percentage:
def calcSeizurePercentage(raw_df, seizure_df):
    return seizure_df['length'].sum() / raw_df.index[-1] #wrong, proably substract start index



    
def analyseForHeuristicPercentage(df, plot=False, cut=True):
    filepath = "~/Projects/Data/interim/filtered/"+df['filename']+".feather"
    #raw = pd.read_csv(filepath,
    #                   sep="\t", header=None, names=['yvalue']);
    raw = load_feather(filepath)
    print('Opened ', df['filename'],".feather")
       
    
    if cut:   
        raw = stripRawData(raw, int(df['start_in_s']), int(df['end_in_s']))             
  #  raw_s = raw.index / 2000
  #  raw.index = raw['s'] = pd.to_timedelta(raw_s, unit='s')
    raw.index = convertIntIndexToTimeDelta(raw)
   
    # resample to 10ms and add variance over 40 ms
    raw_sampled = resampleToMean(raw, '10L')
    raw_sampled["var"] = raw_sampled.rolling(4).var()
     # TODO don't call it s, because it is time with minutes
    raw_sampled['time'] = raw_sampled.index
    
    
    
    min_height = raw_sampled['var'].mean()
    std =  raw_sampled['var'].std()
    
    threshold = min_height+std
    
    peaks, _ = signal.find_peaks(raw_sampled['var'],
                                          height=threshold,
                                          #threshold=min_height,
                                          distance=10,
                                          prominence=min_height,
                                          width=1.5
                                         )
    
    
    
    
    fig = plotPeaks(peaks,
                    raw_sampled,
                    'var',
                    threshold,
                    title=df['filename'],
                    filename=df['filename']+'_allpeaks',
                    nodatetime=True)
 #   fig.savefig(df['filename']+'.pdf')#plot subrange
    
    
    
    
    prominences = signal.peak_prominences(raw_sampled['var'], peaks)[0]
    
    # TODO use with width for sns jointplot
    if plot: plotPeakProminencesHist(prominences,
                    filename=df['filename']+'_peakprominences',
                    nodatetime=True)
    
    
    results_half = signal.peak_widths(raw_sampled['var'], peaks, rel_height=0.5)
    
    if plot: plotPeakProperties(peaks, raw_sampled['var'], prominences, results_half, threshold)
    
    
    if plot: plotPeakWidthHist(results_half[0])
    
    
    if plot: plotPeakDiff(peaks)
    # diff <= 25 seizure
    
    pos_seizure = raw_sampled.iloc[peaks].copy()
    
    if plot: plotPossibleSeizurePeaksHist(pos_seizure)
    
    pos_seizure['peaks'] = peaks
    pos_seizure['length_to_last_peak'] = pos_seizure['peaks'].diff()
    pos_seizure['pos_start'] = np.where((pos_seizure['length_to_last_peak'] > 50), 1, 0)
    #pos_seizure['no_real_start'] = -pos_seizure['pos_start'].diff(periods=-1)
    pos_seizure['pos_end'] = np.where((pos_seizure['length_to_last_peak'].shift(-1) > 25), 1, 0)
    #pos_seizure['no_real_end'] = pos_seizure['pos_end'].diff()
    
    #[(pos_seizure['no_real_start'] != 1) & (pos_seizure['no_real_end'] != 1)]
    cleaned_peaks = pos_seizure
    
    #pos_seizure.plot(kind='hist', y='var', bins=50)
    
    """
    ax = raw_sampled.plot(x='s', y='var', c='orange')
    ax = cleaned_peaks.plot(x='s',y='var', style='.', ax=ax)
    ax = cleaned_peaks[cleaned_peaks['pos_start'] == 1].plot(x='s',y='var', style='.', ax=ax)
    ax = cleaned_peaks[cleaned_peaks['pos_end'] == 1].plot(x='s',y='var', style='.', ax=ax)
    """
    
    # TODO move to own function
    # TODO save percentages
    seizures = seizures.groupSeizures(cleaned_peaks)
    
    length_in_s =  seizures['length'] / pd.Timedelta(seconds = 1)
    
    if plot: plotSeizureLengthHist(length_in_s)
    
    
    if plot: plotSeizurePeaks(raw_sampled,
              cleaned_peaks.loc[seizures['start']],
              cleaned_peaks.loc[seizures['end']])
    
    perc = calcSeizurePercentage(raw_sampled, seizures)
    print(df['filename'], perc*100, '% Seizure time')
    # todo histogram thresholding
    #todo fit curve to prominences
    return perc
    
    #  use over threshold
    # merge if length to before < 1s
    # seizure if length > 1s from first to last


# load data
#C:/Users/thomasdo
# "~/Projects/Data/interim/filtered/2015-09-02T1826.dat" #5:1808 #64.19%
# "~/Projects/Data/interim/filtered/2015-09-02T1928.dat" #4:1805 #75.33%
# "~/Projects/Data/interim/filtered/2018-10-16T1323.dat" #4:1810 #54.99%
# "~/Projects/Data/interim/filtered/2018-10-16T1405.dat" #34:1841 #37.60%! not trustworthy because of problems at end
# "~/Projects/Data/interim/filtered/2018-10-16T1440.dat" #17:1821 #54.33%
# "~/Projects/Data/interim/filtered/2018-10-23T1208.dat" #3:1800 #51.03%
# "~/Projects/Data/interim/filtered/2018-10-23T1242.dat" #7:1811 #61.54%
# "~/Projects/Data/interim/filtered/2019-02-12T1201.dat" #5:1813 #21.33! not trustworthy because missing seizure merge
# "~/Projects/Data/interim/filtered/2019-02-12T1234.dat" #3:1806 #31.34! see above
# Neu:
# 2015-09-02T1826 66.19671568979916  %
# 2015-09-02T1928 79.3982238128743   % !
# 2018-10-16T1323 57.499765192072886 %
# 2018-10-16T1405 38.571094900026615 % !
# 2018-10-16T1440 58.339145190253646 %
# 2018-10-23T1208 51.29417385652143  %
# 2018-10-23T1242 62.387423453470205 %
# 2019-02-12T1201 21.336576594465495 % !
# 2019-02-12T1234 32.501287382543644 % !
# 2019-02-18T1328 66.13524673989353  %
# 2019-02-18T1401 64.03449358201311  %
# 2019-02-20T1154 43.5310022672397   %  
# 2019-02-21T1247 68.13984641591189  % 
# 2019-02-21T1340 77.05743441331046  % !
# 2019-03-06T1330 83.85076227965709  % !
# 2019-03-07T1714 51.48679349775643  %
# 2019-03-07T1745 41.77098279814622  %
def main():
    traces = load_csv('~/Projects/Data/raw/calcium/forGAERSproject/trace_properties.csv', sep=',')
    traces = traces[traces.filename=="2018-10-16T1405"]
    for index, row in traces.iterrows():
        print('Analyse ', row['filename'])
        analyseForHeuristicPercentage(row, plot=True)
    
if __name__ == "__main__": main()



