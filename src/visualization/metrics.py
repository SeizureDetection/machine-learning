import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix

import src.metrics

def plot_mismatches(real_seizures_df, algo_seizures_df):
    m_df = src.metrics.create_mismatch_df(real_seizures_df, algo_seizures_df)
    m_df = src.metrics.remove_too_big_mismatches(m_df)
    m_df = m_df[['mismatch_start', 'mismatch_end']]
    m_df.columns = ['Seizure start', 'Seizure end']
    print(m_df.describe())

    ax = sns.swarmplot(data=m_df,  color="gray")
    sns.violinplot(data=m_df, ax=ax)
    ax.set(ylabel='Mismatch (s)')
    ax.set_title('Mismatch of algorithm seizure to corresponding real seizure')

# From https://gist.github.com/shaypal5/94c53d765083101efc0240d776a23823 (Accessed on 14.11.2019)
# and changed
def plot_confusion_matrix(y_true, y_pred, classes):
    """Prints a confusion matrix, as returned by sklearn.metrics.confusion_matrix, as a heatmap.

    Arguments
    ---------
    confusion_matrix: numpy.ndarray
        The numpy.ndarray object returned from a call to sklearn.metrics.confusion_matrix.
        Similarly constructed ndarrays can also be used.
    classes: list
        An ordered list of class names, in the order they index the given confusion matrix.
    figsize: tuple
        A 2-long tuple, the first value determining the horizontal size of the ouputted figure,
        the second determining the vertical size. Defaults to (10,7).
    fontsize: int
        Font size for axes labels. Defaults to 14.

    Returns
    -------
    matplotlib.figure.Figure
        The resulting confusion matrix figure
    """
    cm = confusion_matrix(y_true, y_pred)
    df = pd.DataFrame(cm, index=classes, columns=classes)
    fig = plt.figure()
    heatmap = sns.heatmap(df, annot=True, fmt="d")
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right')
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right')
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return fig