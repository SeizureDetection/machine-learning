# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 09:50:35 2019

@author: thomasdo
"""
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection

import src.data.seizures as seiz
import src.visualization.visualize as vis


# todo color area behind start and end
# TODO sort by seizure length
# TODO give data_df as series, because it can be filtered signal too
from src.visualization.visualize import save_figure


def plot_all_seizures_stacked(seizures_df, data_series,
                              title='', start_second=pd.Timedelta('-1s')):
    """ start_second gives if there should be an intervall showed before the seizure, too
    Standard is 1s is  showed from before """
    seiz_data_series = seiz.get_ictal_data(data_series,
                                           seizures_df['start'] + start_second,
                                           seizures_df['end'])
#    seiz_data_series = seiz.scale_data(seiz_data_series, seizures_df['start'], seizures_df['start']+pd.Timedelta('2s'))
    adjusted_delta = seiz.adjustTimeDeltaToSeizureStart(seiz_data_series, seizures_df['start'])
    num_subplots = len(seizures_df)
    fig, axs = plt.subplots(num_subplots, 1, sharex=True)
    # Remove horizontal space between axes
    fig.subplots_adjust(hspace=0)
    fig.suptitle(title)
    plt.xlabel('Seconds since seizure start')

    for i in range(num_subplots):
        axs[i].plot(adjusted_delta[i].index.values, adjusted_delta[i].values)
        axs[i].set_yticks([])
        axs[i].grid(True)

    plt.show()


def plot_all_seizures(seizures_df, data_series, title='', start_second=pd.Timedelta('-0.5s'),
                      length=pd.Timedelta('2s'), rescale=False):
    # analog to https://matplotlib.org/gallery/shapes_and_collections/line_collection.html
    #ys = seiz.getPartsOfSeizures(data_series,
    #                             seizures_df['start'] + start_second,
    #                             length=length)
    ys = seiz.get_ictal_data(data_series,
                             seizures_df['start'] + start_second,
                             seizures_df['start'] + length)
    if rescale:
        ys = seiz.standard_scale_data(ys, seizures_df['start'], seizures_df['end'])

    ys = seiz.adjustTimeDeltaToSeizureStart(ys, seizures_df['start'])
    x = ys[0].index.values
    fig, ax = plt.subplots()
    ax.set_xlim(np.min(x), np.max(x))
    min = ys[0][0]
    max = ys[0][0]
    for y in ys:
        min = np.min(y.values) if np.min(y.values) < min else min
        max = np.max(y.values) if np.max(y.values) > max else max

    ax.set_ylim([min, max])
    line_segments = LineCollection([np.column_stack([x, y]) for y in ys], linewidths=0.5)
    numbers = np.arange(len(ys))
    line_segments.set_array(numbers)
    ax.add_collection(line_segments)
    axcb = fig.colorbar(line_segments)
    axcb.set_label('Seizure number')
    ax.set_title(title)
    plt.sci(line_segments)  # This allows interactive changing of the colormap.
    plt.show()


# TODO use https://seaborn.pydata.org/generated/seaborn.lineplot.html#seaborn.lineplot (create Dataframe)
# TODO maybe normalize seizures before from min to max value
def plot_average_seizure(seizures_df, data_series, title='', start_second=pd.Timedelta('-0.5s'),
                         length=pd.Timedelta('2s'), rescale=False, filename=''):


    mean, std = seiz.get_mean_std(seizures_df, data_series, start_second, length, rescale=rescale)
  #  print('y:', y)
  #  x = np.linspace(start_second / pd.Timedelta('1s'), (start_second + length) / pd.Timedelta('1s'), len(y))
    x = mean.index.values
    # TODO fix
   # std = seiz.get_std(seizures_df, data_series)
    fig = plt.figure()
    plt.title(title)
    plt.xlabel('Time since seizure start (s)')
    plt.ylabel('Calcium value')
    plt.plot(x, mean, 'k', color='#CC4F1B')
    plt.fill_between(x, mean - std, mean + std, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
    plt.show()
    if filename:
        vis.save_figure(fig, filename, dirname='../reports/figures/average-seizures/', nodatetime=True)


def plot_powerspectrum_ictal(seizures_df, data_series):
    #TODO fix and use get_ictal_data
   # mean, _ = seiz.get_mean_std(seizures_df, data_series, pd.Timedelta(0))
    ictal_data = seiz.get_ictal_data(data_series, seizures_df['start'], seizures_df['end'])
    as_one_series = pd.concat([s for s in ictal_data])
    # TODO should do fft for each seizure and plot average
    plot_powerspectrum(as_one_series, label='Ictal')
   # plt.psd(mean, 2048, Fs=freq)

def plot_powerspectrum_nonictal(seizures_df, data_series):
    nonictal_data = seiz.get_nonictal_data(data_series, seizures_df['start'], seizures_df['end'])
    as_one_series = pd.concat([s for s in nonictal_data])
    plot_powerspectrum(as_one_series, label='Non-ictal')
    #plt.psd(noise.values, 2048, Fs=freq)

def plot_powerspectrum_ictal_nonictal(seizures_df, data_series):
    plot_powerspectrum_ictal(seizures_df, data_series)
    plot_powerspectrum_nonictal(seizures_df, data_series)
    plt.legend(loc='upper right', fontsize=16)
    plt.yticks([])
    plt.ylabel('Relative PSD')
    plt.xlabel('Frequency (Hz)')

def plot_powerspectrum(data_series, label=None):
    dt = data_series.index[1] - data_series.index[0]
    dt = dt / pd.Timedelta('1s')
    data = data_series.values
    ps = np.abs(np.fft.fft(data)) ** 2
    freq = np.fft.fftfreq(data.size, dt)

  #  freq = freq[freq > 0]
    idx = np.argsort(freq)
    freq = freq[idx]
    ps     = ps[idx]
    plt.grid()
    plt.plot(freq, ps, label=label)



def plot_seizure_comparison(data_series, seizures_df_0, seizures_df_1):
    fig, ax = plt.subplots(2, sharex=True, sharey=True)
    ax[0].plot(data_series.index.values, data_series, c='b')
    ax[0].plot(seizures_df_0['start'], data_series.loc[seizures_df_0['start']], 'go')
    ax[0].plot(seizures_df_0['end'], data_series.loc[seizures_df_0['end']], 'ro')

    ax[1].plot(data_series.index.values, data_series, c='b')
    ax[1].plot(seizures_df_1['start'], data_series.loc[seizures_df_1['start']], 'go')
    ax[1].plot(seizures_df_1['end'], data_series.loc[seizures_df_1['end']], 'ro')

    def numfmt(x, pos):  # TODO export to
        s = '{}'.format(pd.to_timedelta(x, unit='ns').total_seconds())
        return s

    ax[1].xaxis.set_major_formatter(tkr.FuncFormatter(numfmt))

    for index, row in seizures_df_0.iterrows():
        ax[0].axvspan(row['start'] / pd.Timedelta('1ns'), row['end'] / pd.Timedelta('1ns'), color='y', alpha=0.3)

    for index, row in seizures_df_1.iterrows():
        ax[1].axvspan(row['start'] / pd.Timedelta('1ns'), row['end'] / pd.Timedelta('1ns'), color='y', alpha=0.3)





def plotSeizureLengthHist(length_s, title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Seizure length')
    length_s_in_s = length_s / pd.Timedelta(seconds=1)  # TODO move to plot
    ax =  length_s_in_s.plot.hist(bins=18, range=(1, 10))
    plt.xlabel('s')
    plt.ylabel('frequency')
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax