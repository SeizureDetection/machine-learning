import datetime
import matplotlib.ticker as tkr
import pandas as pd

def dateformater():
    def numfmt(x, pos):  # TODO export to
        s = '{}'.format(pd.to_timedelta(x, unit='ns').total_seconds())
        return s
    return tkr.FuncFormatter(numfmt)


def save_figure(figure, filename, dirname='', nodatetime=False):
    # if savefigure is changed, change it in FRETup project, too
    timestamp = ''
    if not nodatetime:
        timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H%M%S") + '_'

    figure.savefig(dirname + timestamp + filename + '.pdf')
    figure.savefig(dirname + timestamp + filename + '.png')