from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import src.data.prepare
import src.features.feature_selection as feat_sel
import src.features.build_features as feat_build

def get_X(feature_df, data_features, scale=True):
    X = feature_df[data_features]
    if scale:
        X = StandardScaler().fit_transform(X)
    return X

def get_X_y_data_df(name):
    data_df = src.data.prepare.load_data(name, type='filtered', load_seizures=True)
    feature_df = feat_build.calc_features(
        data_df['data'], data_df['real_seizures_df'], group_interval='1s')
    a4_feature_df, d4_feature_df, d3_feature_df, d2_feature_df, d1_feature_df = feat_sel.calc_wavelet_features_df(data_df['data'],
                                                                                    data_df['real_seizures_df'],
                                                                                    only_best=False)
    feature_df = pd.concat([feature_df, a4_feature_df, d4_feature_df, d3_feature_df, d2_feature_df, d1_feature_df],
                           axis=1)
    Xi = get_X(feature_df, feat_build.get_feature_names(wavelet_features=True))#feat_build.get_best_feature_names())
    yi = get_y(feature_df, 'is_seiz')
    return Xi, yi, data_df['data']


def get_X_y_for(names, scale=True):
    X = [];
    y = [];
    for name in names:
        Xi, yi, _ = get_X_y_data_df(name)
        X.append(Xi)
        y.append(yi)
    X = np.concatenate(X, axis=0)
    y = np.concatenate(y, axis=0)
    return X, y


def get_y(feature_df, y_name, as_int=True):
    y = feature_df[y_name]
    if as_int:
        return [int(yi) for yi in y]
    return y


def train_test_split(X, y):
    X_train, X_test, y_train, y_test = tts(X, y, test_size=0.3,
                                                        random_state=42, shuffle=False)
    return X_train, X_test, y_train, y_test


def get_train_trace_names():
    return ['2016-07-11T1513', '2016-07-26T1107', '2018-10-16T1323', '2018-10-23T1208']


def get_test_trace_names():
    names = src.data.prepare.listValidatorFiles()
    return [name for name in names if name not in get_train_trace_names()]
    #return set(names)-set(get_fit_trace_names())


def plot_features(feature_df, features, by='s_type'):
    for name in features:
        plt.figure()
        ax = sns.violinplot(x=by, y=name, data=feature_df)
        ax.set_title(name)
