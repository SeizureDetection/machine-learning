import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import src.data.prepare as prep
from src.features import build_features


def calc_avg_feature_corr(names, plot=False):
    corr_df = None
    for i, name in enumerate(names):
        print('Run on: ', name)
        data_df = prep.load_data(name, type='filtered', load_seizures=True)
        feature_df = build_features.calc_features(data_df['data'], data_df['real_seizures_df'], group_interval='0.2s')
        if i == 0:
            corr_df = feature_df.corr()
        else:
            corr_df = corr_df + feature_df.corr()
    corr_df = corr_df / len(names)
    if plot:
        sns.heatmap(corr_df, annot=True, fmt=".2f", cmap='RdBu_r')
        plt.show()
    return corr_df

