import numpy as np
import pandas as pd
import scipy.stats


import src.data.seizures as seiz
import src.features.wavelets
import src.data.prepare
import pywt


def calc_features(data_df, real_seizures, group_interval='1s'):
    # group by half second
    # calc each feature for every half second
    # data_df[] = data_df.index // pd.to_timedelta('0.5s')
    data_df['seizure'] = seiz.markSeizures(data_df, real_seizures)
    data_df['seiz_bef'] = data_df.seizure.shift(fill_value=0)


    grouped = group_data(data_df, group_interval)
    s_type = grouped['seizure'].agg(seizure_types).rename('s_type')
    is_seiz = s_type.copy().rename('is_seiz')
    is_seiz[is_seiz > 0] = 1
   # is_seiz[is_seiz > 2.1] = 0 #we take seiz start and end not as seizure
   # is_seiz[is_seiz < 1.9] = 0

    feature_df = pd.concat((
        grouped['yvalue'].agg([np.sum,
                               np.mean,
                               np.var,
                               np.max,
                               manhattan_length,
                               entropy,
                               rms,
                               low_freq_value,
                               mid_freq_value,
                               high_freq_value]), #np.min is unimportant
        s_type,
        s_type.shift(fill_value=0.0).rename('s_type_bef'),
        is_seiz), axis=1)
    return feature_df


def group_data(data_df_index, group_interval='1s'):
    return data_df_index.groupby(lambda x: x // pd.to_timedelta(group_interval))

def add_y_pred_to_data(data_df, y_pred, group_interval='1s'):#TODO better rename to resize_y_pred
    grouped = group_data(data_df, group_interval)
    return y_pred[grouped.ngroup()]


def get_feature_names(wavelet_features=False):
    feat_names = np.array(['sum', 'mean', 'var', 'amax',
                     'manhattan_length', 'entropy',
                     'rms',
                     'low_freq_value',
                     'mid_freq_value',
                     'high_freq_value'])#, 's_type', ])#'s_type_bef'
    if wavelet_features:
        feat_names = np.append(feat_names, src.features.wavelets.prepend_names(feat_names))
    return feat_names

def get_best_feature_names(wavelet_features=True):
    best_feat_names = np.array(['a4_manhattan_length','manhattan_length', 'a4_low_freq_value', 'a4_entropy', 'entropy',
                                'low_freq_value', 'var', 'd4_manhattan_length', 'a4_var', 'a4_mid_freq_value', 'mid_freq_value',
                                'd4_rms', 'high_freq_value', 'a4_high_freq_value', 'd3_manhattan_length', 'd4_amax', 'd4_var',
                                'a4_amax', 'amax', 'a4_mean', 'a4_sum', 'd4_amax', 'rms', 'mean'])#24 features
    return best_feat_names

def get_feature_num():
    return len(get_feature_names())

def is_seizure(seizure_series):
    """
    Returns series where group is 1 if is seizure_start, end, or inbetween
    :param seizure_series:
    :return:
    """
    return pd.Series(seizure_series > 0.0).all()


def seizure_types(seizure_series):
    """
    Adds a value, depending if seizure_series contains a seizure (2), no seizure at all (0) or start (1) or end (3) of seizure
    :param seizure_series:
    :return:
    """
    # asserts that the group is less than 1 second (0.5 seconds)
    start, end = seizure_series.values[[0,-1]]
    if start == end:
        if start == 0.0:
            return 0  # no seizure
        if start == 1.0:
            return 2  # seizure
    #else:
    if start == 0.0 and end == 1.0:
        return 1  # seizure start
    if start == 1.0 and end == 0.0:
        return 3  # seizure end
    raise ValueError('Unexpected values in tag_points', start, end)

def manhattan_length(y_series):#faster to calculate than square length
    y = y_series.values
    diff = y[1:] - y[:-1]
    abs = np.absolute(diff)
    return np.sum(abs)


def entropy(y_series):
    ent = scipy.stats.entropy(y_series.values)
    if np.isinf(ent).any() or np.isnan(ent).any():
        return np.zeros_like(ent)
    return ent

def rms(y_series):
    """Root mean square"""
    return np.sqrt(np.mean(y_series.values**2))


def powerspectrum(y_series):
    dt = y_series.index[1] - y_series.index[0]
    dt = dt / pd.Timedelta('1s')
    data = y_series.values
    ps = np.abs(np.fft.fft(data)) ** 2
    freq = np.fft.fftfreq(data.size, dt)
    idx = np.argsort(freq)
    freq = freq[idx]
    ps = ps[idx]
    return freq, ps

def calc_freq_value(y_series, min_freq, max_freq):
    freq, ps = powerspectrum(y_series)
    # minimize freq and ps for faster lookup
#    roi = (freq > 4) & (freq < 18)
 #   freq = freq[roi]
  #  ps = ps[roi]
    range = (freq >= min_freq) & (freq <= max_freq)
    freq_value = np.max(ps[range])
    return freq_value

def low_freq_value(y_series):
    return calc_freq_value(y_series, 4.5, 6)

def mid_freq_value(y_series):
    return calc_freq_value(y_series, 9.5, 12)

def high_freq_value(y_series):
    return calc_freq_value(y_series, 14.5, 17)






