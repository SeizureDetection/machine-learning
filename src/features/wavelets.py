import numpy as np
import pandas as pd
import pywt
import matplotlib.pyplot as plt
import seaborn as sns


def calc_wavelets(data_s):
    cA4, cD4, cD3, cD2, cD1 = pywt.wavedec(data_s, 'db4', mode='per', level=4)
    return cA4, cD4, cD3, cD2, cD1

def reconstruct_coeffs(index, cA4, cD4, cD3, cD2, cD1):
    a4 = pywt.waverec([cA4, np.zeros_like(cD4), np.zeros_like(cD3), np.zeros_like(cD2), np.zeros_like(cD1)], 'db4',
                      mode='per')
    a4 = pd.DataFrame(a4, index=index, columns=['yvalue'])
    d4 = pywt.waverec([np.zeros_like(cA4), cD4, np.zeros_like(cD3), np.zeros_like(cD2), np.zeros_like(cD1)], 'db4',
                      mode='per')
    d4 = pd.DataFrame(d4, index=index, columns=['yvalue'])
    d3 = pywt.waverec([np.zeros_like(cA4), np.zeros_like(cD4), cD3, np.zeros_like(cD2), np.zeros_like(cD1)], 'db4',
                      mode='per')
    d3 = pd.DataFrame(d3, index=index, columns=['yvalue'])
    d2 = pywt.waverec([np.zeros_like(cA4), np.zeros_like(cD4), np.zeros_like(cD3), cD2, np.zeros_like(cD1)], 'db4',
                      mode='per')
    d2 = pd.DataFrame(d2, index=index, columns=['yvalue'])
    d1 = pywt.waverec([np.zeros_like(cA4), np.zeros_like(cD4), np.zeros_like(cD3), np.zeros_like(cD2), cD1], 'db4',
                      mode='per')
    d1 = pd.DataFrame(d1, index=index, columns=['yvalue'])
    return a4, d4, d3, d2, d1


def get_wavelet_traces(data_s):
    cA4, cD4, cD3, cD2, cD1 = calc_wavelets(data_s)
    a4, d4, d3, d2, d1 = reconstruct_coeffs(data_s.index, cA4, cD4, cD3, cD2, cD1)
    return a4, d4, d3, d2, d1


def prepend_names(names):
    a4_names = ['a4_' + name for name in names]
    d4_names = ['d4_' + name for name in names]
    d3_names = ['d3_' + name for name in names]
    d2_names = ['d2_' + name for name in names]
    d1_names = ['d1_' + name for name in names]
    return a4_names, d4_names, d3_names, d2_names, d1_names


def plot_reconstruction(raw, a4, d4, d3, d2, d1, xlim=None):
    fig, ax = plt.subplots(6, 1, sharex=True)
    from matplotlib import ticker
 #   t = np.arange(0, len(a4))
    t1 = raw.index.total_seconds()
    t2 = a4.index.total_seconds()
 #   if xlim is not None:
 #       print(t1, type(t1), t2, type(t2))
 #       t1.array = t1.array-xlim[0]
 #       t2.array = t1.array-xlim[0]
    ax[0].plot(t1, raw)
    ax[0].set_ylim(0.14,0.170)
    ax[0].set_ylabel('y')
    ax[1].plot(t2, a4)
    ax[1].set_ylim(0.14,0.170)
    ax[1].set_ylabel('a4')
    ax[2].plot(t2, d4)
  #  ax[2].set_ylim(-9e-4,9e-4)
    ax[2].set_ylabel('d4')
    ax[3].plot(t2, d3)
  #  ax[3].set_ylim(-9e-5,9e-5)
    ax[3].set_ylabel('d3')
    ax[4].plot(t2, d2)
  #  ax[4].set_ylim(-9e-6,9e-6)
    ax[4].set_ylabel('d2')
    ax[5].plot(t2, d1)
    ax[5].set_ylim(-3e-3,3e-3)
    ax[5].set_ylabel('d1')
    ax[5].set_xlabel('Time (s)')
    ax[0].set_xlim(xlim)
   # for i in range(0,6):
       # ax[i].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
    #    ax[i].yaxis.set_label_position("right")
    for i in range(2,6):
        ax[i].ticklabel_format(axis='y', style='sci', scilimits=(-10,-9), useOffset=True, useLocale=False)
        pass
    plt.tight_layout()

