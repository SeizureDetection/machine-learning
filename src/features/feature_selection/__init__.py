import numpy as np
import pandas as pd
import sklearn.feature_selection
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import LinearSVC
import src.data.prepare
import src.features as feat
import src.features.wavelets
import src.features.build_features
import src.features.feature_selection


#partly from https://scikit-learn.org/stable/modules/feature_selection.html

def variance_based(X, y, features, threshold):
    """
    :param X:
    :param y:
    :param features:
    :param threshold:
    :return:
    """
    sel = sklearn.feature_selection.VarianceThreshold(threshold=threshold)
    sel = sel.fit(X)
    return features[sel.get_support()], list(zip(features, sel.variances_))


def f_classif_based(X, y, features, max_features):
    sel = sklearn.feature_selection.SelectKBest(sklearn.feature_selection.f_classif, k=max_features)
    sel.fit(X, y)
    return features[sel.get_support()], list(zip(features, sel.scores_))

def tree_based(X,y, features, threshold=None, max_features=None):
    clf = ExtraTreesClassifier(n_estimators=1000, n_jobs=-1)
    # could use threshold or nax_features parameters
    clf = clf.fit(X, y)
    model = sklearn.feature_selection.SelectFromModel(clf, prefit=True, threshold=threshold, max_features=max_features)
    return features[model.get_support()]#, list(zip(features, model.threshold_))



def RFECV(X, y, features):
    """
    X, y should be used be standard scaled
    :param X:
    :param y:
    :param features:
    :return:
    """
    clf = ExtraTreesClassifier(n_estimators=100, n_jobs=-1)
    rfecv = sklearn.feature_selection.RFECV(estimator=clf, step=1, cv=StratifiedKFold(2),
                                            scoring='accuracy', min_features_to_select=10)
    rfecv.fit(X, y)
    return features[rfecv.support_], list(zip(features, rfecv.ranking_))


def get_best_features_for(names):
    feat_names = feat.build_features.get_feature_names(wavelet_features=True)
    feat_count = pd.Series(0, index=feat_names)
    # TODO use RandomizedSearchCV or GRIDSearch
    for i, name in enumerate(names):
        print('Run on: ', name)
        data_df = src.data.prepare.load_data(name, type='filtered', load_seizures=True)
        feat_count_new = get_best_features(data_df['data'], data_df['real_seizures_df'])
        feat_count += feat_count_new
    return feat_count.sort_values(ascending=False)

def get_best_features(data_df, real_seizures_df):
    feature_df = src.features.build_features.calc_features(
        data_df, real_seizures_df, group_interval='1s')
    feat_names = feat.build_features.get_feature_names(wavelet_features=True)
    feat_count = pd.Series(0, index=feat_names)

    a4_feature_df, d4_feature_df, d3_feature_df, d2_feature_df, d1_feature_df = calc_wavelet_features_df(data_df, real_seizures_df)
    feature_df = pd.concat([feature_df, a4_feature_df, d4_feature_df, d3_feature_df, d2_feature_df, d1_feature_df], axis=1)
    X = feat.get_X(feature_df, feat_names, scale=True)
    y = feat.get_y(feature_df, 's_type')
    fts, _ = variance_based(X, y, feat_names, threshold=1e-5)
    feat_count.loc[fts] = feat_count.loc[fts] + 1
    fts, _ = f_classif_based(X, y, feat_names, max_features=10) #4 if raw
    feat_count.loc[fts] = feat_count.loc[fts] + 1
    fts = tree_based(X, y, feat_names, threshold=-np.inf, max_features=10)#3 if raw
    feat_count.loc[fts] = feat_count.loc[fts] + 1
    fts, _ = RFECV(X, y, feat_names)
    feat_count.loc[fts] = feat_count.loc[fts] + 1
    return feat_count.sort_values(ascending=False)


def calc_wavelet_features_df(data_df, real_seizures_df, only_best=False):
    a4, d4, d3, d2, d1 = feat.wavelets.get_wavelet_traces(data_df['yvalue'].resample('10ms').mean())
    a4_feature_df = src.features.build_features.calc_features(
        a4, real_seizures_df, group_interval='1s')
    d4_feature_df = src.features.build_features.calc_features(
        d4, real_seizures_df, group_interval='1s')
    d3_feature_df = src.features.build_features.calc_features(
        d3, real_seizures_df, group_interval='1s')
    if not only_best:
        d2_feature_df = src.features.build_features.calc_features(
          d2, real_seizures_df, group_interval='1s')
        d1_feature_df = src.features.build_features.calc_features(
          d1, real_seizures_df, group_interval='1s')
    a4_feature_df.columns = 'a4_' + a4_feature_df.columns
    d4_feature_df.columns = 'd4_' + d4_feature_df.columns
    d3_feature_df.columns = 'd3_' + d3_feature_df.columns
    if not only_best:
        d2_feature_df.columns = 'd2_' + d2_feature_df.columns
        d1_feature_df.columns = 'd1_' + d1_feature_df.columns
    if not only_best:
        return a4_feature_df, d4_feature_df, d3_feature_df, d2_feature_df, d1_feature_df
    return a4_feature_df, d4_feature_df, d3_feature_df



