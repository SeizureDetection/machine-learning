# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:56:32 2019

@author: thomasdo
"""
from bisect import bisect_left
from enum import IntEnum

import numpy as np
import pandas as pd

import src.data.prepare as prep
import src.data.seizures as seizures
from src.config import mainconfig
from src.data.prepare import load_real_seizures
import src.algorithms


class Classification(IntEnum):
    TN = 0
    FN = 1
    FP = 2
    TP = 3

    def __str__(self):
        return {
            Classification.TN: 'TN',
            Classification.FN: 'FN',
            Classification.FP: 'FP',
            Classification.TP: 'TP',
        }[self.value]


def analyse(data_series, real_seizures_df, algo_seizures_df):
    real_seizure_series = seizures.markSeizures(data_series, real_seizures_df)
    algo_seizure_series = seizures.markSeizures(data_series, algo_seizures_df)
    # type  3 TP 2 FP 1 FN  0 TN, see classification enum above
    classification_series = real_seizure_series + 2 * algo_seizure_series
    return classification_series


def classification_percentages(classification_series):
    count_df = pd.DataFrame(classification_series.value_counts(normalize=True))
    count_df['type'] = [str(Classification(i)) for i in count_df.index.values]
    count_df = count_df.set_index('type')
    count_df.columns = ['percentage']
    return count_df


def real_seiz_perc(classification_series):
    count_series = classification_series.value_counts()
    return (count_series[Classification.TP] + count_series[Classification.FN]) / count_series.sum()


def algo_seiz_perc(classification_series):
    count_series = classification_series.value_counts()
    return (count_series[Classification.TP] + count_series[Classification.FP]) / count_series.sum()


def accuracy(classification_series):
    count_series = classification_series.value_counts()
    return (count_series[Classification.TP] + count_series[Classification.TN]) / count_series.sum()


def sensitivity(classification_series):
    count_series = classification_series.value_counts()
    return count_series[Classification.TP] / (count_series[Classification.TP] + count_series[Classification.FN])


def specifity(classification_series):
    count_series = classification_series.value_counts()
    return count_series[Classification.TN] / (count_series[Classification.TN] + count_series[Classification.FP])


def negativePredictiveValue(classification_series):
    count_series = classification_series.value_counts()
    return count_series[Classification.TN] / (count_series[Classification.TN] + count_series[Classification.FN])


def positivePredictiveValue(classification_series):
    count_series = classification_series.value_counts()
    return count_series[Classification.TP] / (count_series[Classification.TP] + count_series[Classification.FP])




def matchSeizures(real_seizures_df, algo_seizures_df):
    # adapted from https://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value/12141511#12141511
    def take_closest(myList, myNumber):
        """
        Assumes myList is sorted. Returns closest value to myNumber.

        If two numbers are equally close, return the smallest number.
        """
        pos = bisect_left(myList, myNumber)
        if pos == 0:
            return pos
        if pos == len(myList):
            return pos - 1
        before = myList[pos - 1]
        after = myList[pos]
        if after - myNumber < myNumber - before:
            return pos  # after
        else:
            return pos - 1  # before

    real_seiz_df = real_seizures_df.drop('length', axis=1)
    real_seiz_df.columns = ['real_start', 'real_end']
    real_seiz_nr_to_algo_seiz_nr = real_seiz_df.apply(
        lambda x: [take_closest(algo_seizures_df['start'], x['real_start']),
                   take_closest(algo_seizures_df['end'], x['real_end'])], axis=1, result_type='expand')
    real_seiz_nr_to_algo_seiz_nr.columns = ['algo_start_nr', 'algo_end_nr']
    return real_seiz_nr_to_algo_seiz_nr


def create_mismatch_df(real_seizures_df, algo_seizures_df):  # TODO extract to another file
    df = matchSeizures(real_seizures_df, algo_seizures_df)
    df['real_start'] = real_seizures_df['start']
    df['real_end'] = real_seizures_df['end']
    algo_start = algo_seizures_df.iloc[df['algo_start_nr'].values]['start'].to_frame()
    algo_start['real_s_nr'] = df.index.values
    algo_start = algo_start.set_index('real_s_nr')
    algo_end = algo_seizures_df.iloc[df['algo_end_nr'].values]['end'].to_frame()
    algo_end['real_s_nr'] = df.index.values
    algo_end = algo_end.set_index('real_s_nr')
    df['algo_start'] = algo_start
    df['algo_end'] = algo_end
    df['mismatch_start'] = df['algo_start'] - df['real_start']
    df['mismatch_end'] = df['algo_end'] - df['real_end']
    df['mismatch_start'] = df['mismatch_start'].dt.total_seconds()
    df['mismatch_end'] = df['mismatch_end'].dt.total_seconds()
    return df

def remove_too_big_mismatches(mismatch_df):
    m_df = mismatch_df
    m_df = m_df[(np.abs(m_df['mismatch_start']) < 2) & (np.abs(m_df['mismatch_end']) < 2)]
    m_df = m_df.reset_index()
    return m_df


def correct_seizures(algo_seizures_df, mismatch_df):
    common = mismatch_df.loc[mismatch_df['algo_start_nr'] == mismatch_df['algo_end_nr']]
    return algo_seizures_df.loc[(algo_seizures_df.index.isin(common['algo_start_nr']))]


def partly_correct_seizures(algo_seizures_df, mismatch):
    return algo_seizures_df.loc[(algo_seizures_df.index.isin(mismatch['algo_start_nr']) ^
                                 algo_seizures_df.index.isin(mismatch['algo_end_nr']))]


def wrong_seizures(algo_seizures_df, mismatch_df):
    "Wrong seizures are seizures from the algorithm which are not in the mismatch df, not even partly"
    # shows also seizures which are within a real seizure at the moment, # TODO remove within seizures
    wrong_seizures_df = algo_seizures_df.drop(partly_correct_seizures(algo_seizures_df, mismatch_df).index) \
        .drop(correct_seizures(algo_seizures_df, mismatch_df).index)
    return wrong_seizures_df


def within_seizures(algo_seizures_df, mismatch_df):
    # TODO seizures with party correct start and and end and seizures within
    wrong_seizures_df = algo_seizures_df.drop(partly_correct_seizures(algo_seizures_df, mismatch_df).index) \
        .drop(correct_seizures(algo_seizures_df, mismatch_df).index)
    # isSeizure = pd.Series # TODO remove
    #  for index, row in wrong_seizures_df.iterrows():
    #      mismatch_df.loc[
    #              ((mismatch_df['real_start'] <= row['start'])
    #              & (mismatch_df['real_end'] >= row['end']))] = 1
    return 'TODO'  # TODO


def print_report(report_s):
    print(report_s)


def report(name, algorithm_name, algo_seizures_df, config=None, data_s=None, runtime=None):
    report_s = pd.Series([name, algorithm_name], index=[['name', 'algorithm_name']])
    if data_s is None:
        data_s = prep.load_data(name, 'filtered', load_seizures=True)
    raw = data_s['data']['yvalue']
    if 'real_seizures_df' in data_s:
        real_seizures_df = data_s['real_seizures_df']
    else:
        real_seizures_df = load_real_seizures(name)
    report_s['algo_runtime_seconds'] = runtime
    report_s['real_seconds'] = (raw.index[-1] - raw.index[0]) / np.timedelta64(1, 's')
    report_s['real_count'] = len(real_seizures_df)
    report_s['algo_count'] = len(algo_seizures_df)
    classification_s = analyse(raw, real_seizures_df, algo_seizures_df)
    perc = classification_percentages(classification_s)
    report_s['TP'] = perc.loc['TP'][0] # TODO sometime some of the values are not set
    report_s['TN'] = perc.loc['TN'][0]
    report_s['FN'] = perc.loc['FN'][0]
    report_s['FP'] = perc.loc['FP'][0]
    report_s['accuracy'] = accuracy(classification_s)
    report_s['sensitivity'] = sensitivity(classification_s)
    report_s['specificity'] = specifity(classification_s)
    report_s['negative_predictive_value'] = negativePredictiveValue(classification_s)
    report_s['positive_predictive_value'] = positivePredictiveValue(classification_s)

    mismatch_df = create_mismatch_df(real_seizures_df, algo_seizures_df)
    report_s['mismatch_start'] = mismatch_df['mismatch_start'].describe()[['mean', 'std', 'min', 'max']]
    report_s['mismatch_end'] = mismatch_df['mismatch_end'].describe()[['mean', 'std', 'min', 'max']]
    report_s['corr_s'] = len(correct_seizures(algo_seizures_df, mismatch_df))
    report_s['partly_corr_s'] = len(partly_correct_seizures(algo_seizures_df, mismatch_df))
    report_s['within_s'] = len(within_seizures(algo_seizures_df, mismatch_df))
    report_s['wrong_s'] = len(wrong_seizures(algo_seizures_df, mismatch_df))
    if config is not None and config['save']:
        filepath = mainconfig['project']['dir'] + "/reports/algorithm_summary/" + algorithm_name + "/" + name + ".dat"
        report_s.to_csv(filepath, header=False, sep='\t') # TODO save mismatch separateky
        #report_s.to_frame().reset_index().to_feather(filepath)
    return report_s


def give_shortinfo(names, algorithm):
    acc = sens = spec =  paz = prz = 0
    for name in names:
        print(name)
        ac,  se, sp, p_algo_seiz, p_real_seiz, seiz_num = get_metrics_for(algorithm, name)
        print('% seiz (algo, real)', p_algo_seiz, p_real_seiz)
        print(ac, se, sp)
        acc += ac
        sens += se
        spec += sp
        paz += p_algo_seiz
        prz += p_real_seiz
    #    mismatch_df = val.create_mismatch_df( real_seizures_df, algo_seizures_df)
    #    print(mismatch_df['mismatch_start'].describe())
    #    print(mismatch_df['mismatch_end'].describe())
    n = len(names)
    acc /= n
    sens /= n
    spec /= n
    paz /= n
    prz /= n
    print('% seiz all (algo, real)', paz, prz)
    print('Avg. acc.:', acc)
    print('Avg. sens.:', sens)
    print('Avg. spec.:', spec)


def get_metrics_for(algorithm, name):
    algo_seizures_df = algorithm.load(name)
    data = prep.load_data(name, 'raw', load_seizures=True)  # TODO load only seizures_df
    raw = data['data']
    real_seizures_df = data['real_seizures_df']
    #   print(real_seizures_df.tail())
    #   print(algo_seizures_df.tail())
    #  report = val.report(name, 'variance_algorithm', algo_seizures_df)
    classification = analyse(raw, real_seizures_df, algo_seizures_df)
    p_real_seiz = real_seiz_perc(classification) * 100
    p_algo_seiz = algo_seiz_perc(classification) * 100
    ac = accuracy(classification) * 100
    se = sensitivity(classification) * 100
    sp = specifity(classification) * 100
    seiz_num = len(algo_seizures_df)
    return ac, se, sp, p_algo_seiz, p_real_seiz, seiz_num


def create_metrics_df(names):
    df_list = []
    traces = prep.loadTraceProperties()
    traces['length'] = traces['end_in_s'] - traces['start_in_s']
    for name in names:
        for algo_name, algo in src.algorithms.get_algo_dict().items():
            ac, se, sp, p_algo_seiz, p_real_seiz, seiz_num = get_metrics_for(algo, name)
            df_list.append([name, algo_name, ac, se, sp, seiz_num, p_algo_seiz])

        rs_df = prep.load_real_seizures(name)
        rs_df['length'] = rs_df['end'] - rs_df['start']
        real_num = len(rs_df)
        real_perc =  rs_df['length'].sum().total_seconds()/traces.loc[name, 'length']*100
        df_list.append([name, 'manual', None, None, None, real_num, real_perc])
    return pd.DataFrame.from_records(df_list, columns= ['trace', 'algo', 'accuracy', 'sensitivity', 'specificity', 'seiz_num', 'seiz_perc'])

