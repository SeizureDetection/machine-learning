import graphviz
from sklearn import tree
from sklearn.metrics import accuracy_score


def decision_tree(X_train, y_train, X_test, y_test, data_features):

    clf = tree.DecisionTreeClassifier(min_samples_leaf=10,
                                       max_depth=5, random_state=42)
    clf = clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print('Decision tree acc.:', accuracy)
    importances = list(zip(data_features, clf.feature_importances_))
    print('Feature importance')
    print(importances)
    dot_data = tree.export_graphviz(clf, out_file=None,
                                    filled=True, rounded=True,
                                    feature_names=data_features,
                                    class_names=['normal', 'seiz_start', 'seizure', 'seiz_end'],
                                    special_characters=True)
    tree.plot_tree(clf, filled=True, feature_names=data_features)
   # graph = graphviz.Source(dot_data)
    return clf
