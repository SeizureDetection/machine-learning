# -*- coding: utf-8 -*-
import glob
import logging
import os
from pathlib import Path

import pandas as pd
from dotenv import find_dotenv, load_dotenv
from scipy import signal


def apply50HzFilter(files):
    """ Asserts sampling freq = 2000"""
    sos = signal.butter(4, 50, fs=2000, output='sos')
    # TODO use notch filter
    for path in files:
        print(path)
        file = os.path.basename(path)
        df = pd.read_csv(path,
                         sep="\t", header=None, names=['yvalue']);
        print("Transform", file)
        filt_sig = signal.sosfilt(sos, df.yvalue)
        b, a = signal.iirnotch(50.0, 30, fs=2000.0)
        df["filtered"] = signal.filtfilt(b, a, filt_sig)
        filepath_save = "../../../Data/interim/filtered/" + file
        df.to_csv(filepath_save, columns=["filtered"], header=False, index=False)


def csvToFeather(files):
    for file in files:
        filename = os.path.splitext(file)[0]
        print('Transform ', filename, ' from .dat to .feather')
        df = pd.read_csv(file, names=['yvalue'])
        df.to_feather(filename + '.feather')


# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
def main():  # input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../interim).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    raw_files = glob.glob("../../../Data/raw/calcium/*.dat")
    print(raw_files)
    apply50HzFilter(raw_files)
    interimCSVFiles = glob.glob("../../../Data/interim/filtered/*.dat")
    csvToFeather(interimCSVFiles)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
