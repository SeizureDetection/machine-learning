# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 12:10:38 2019

@author: thomasdo
"""
import glob
import os

import pandas as pd

from src.config import mainconfig
from src.data import seizures as seizures


def load_feather(filepath):
    df = pd.read_feather(filepath)
    return df


def load_csv(filepath, sep="\t", header='infer', names=None, index_col=None):
    df = pd.read_csv(filepath, comment='#', header=header, names=names, sep=sep, index_col=index_col)
    return df


def loadTraceProperties():
    df = load_csv(mainconfig['calcium_data']['trace_properties_file_path'], sep=",", index_col=0)
    return df


def stripDFByName(raw_df, name):
    prop_df = loadTraceProperties()
    prop = prop_df.loc[name]
    # skip everything before laser turned on and after laser turned off
    raw_df = stripRawData(raw_df, prop['start_in_s'], prop['end_in_s'])
    return raw_df

def load_data(name, type, load_seizures=False):
    if type == 'raw':
        data = _load_raw_data(name)
    elif type == 'filtered':
        data = _load_filtered_data(name)
    else:
        raise ValueError('type argument not raw or fitlered')
    data_s = pd.Series() # TODO eport as create_data_df
    data_s['name'] = name
    data_s['type'] = type
    data_s['freq'] = 2000
    data_s['data'] = data
    if load_seizures: # TODO fix bug
        data_s['real_seizures_df'] = load_real_seizures(name)
    return data_s


def _load_raw_data(name):
    raw = load_csv(mainconfig["calcium_data"]["raw_files_dir"] + name + '.dat', names=['yvalue'], sep="\t",
                   header=None, )  # TODO rename to loadDatacsv AND USE PARAMETES IN FUNCTION
    raw = prepare_data_df(name, raw)
    return raw


def _load_filtered_data(name):
    filtered = load_feather(mainconfig["calcium_data"]["filtered_files_dir"] + name + '.feather')  # TODO function loadfiltered
    filtered = prepare_data_df(name, filtered)
    return filtered


def prepare_data_df(name, raw):
    # skip everything before laser turned on and after laser turned off
    raw = stripDFByName(raw, name) # TODO call after index change and striprawdata can use new index then
    raw.index = convertIntIndexToTimeDelta(raw)
    return raw

def stripRawData(raw_df, start_second=0, end_second=-1, freq=2000):
    """ freq is the sampling freq in 1/s """
    start_value = start_second * freq
    end_value = end_second * freq
    # prepare data
    if end_second < 0:
        return raw_df[start_value]
    return raw_df[start_value:end_value]


def resampleToMean(df, rule='10L'):
    """ standard to 10 ms """
    return df.resample('10L').mean()

# TODO check if used and can be replaced
def convertFloatToTimeDelta(series, freq=2000):
    return pd.to_timedelta(series / freq, unit='s')

# TODO check if used and can be replaced
def convertTimeDeltaToFloat(series, freq=2000):
    return convertTimeDeltaToSecond(series, freq=freq) * freq


def convertIntIndexToTimeDelta(raw_df, freq=2000):
    return pd.to_timedelta(raw_df.index / freq, unit='s')


def convertTimeDeltaToSecond(series, freq=2000):
    return series.values / pd.to_timedelta('1s')


def listFilteredFiles():
    return [os.path.splitext(os.path.basename(x))[0] for x in
            sorted(glob.glob('../../Data/interim/filtered/*.feather'))]


def listValidatorFiles():
    return [os.path.splitext(os.path.basename(x))[0] for x in sorted(glob.glob('../../Data/raw/validators/*.dat'))]


def load_real_seizures(name):
    real_seizures_df = seizures.loadSeizures("~/Projects/Data/raw/validators/" + name + ".dat")
    # TODO use groupSeizures and merge to get only official  seizures with length > 1s and pause > 1s between them
    return real_seizures_df