# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 17:08:53 2019

@author: thomasdo
"""
import numpy as np
import pandas as pd

import src.data.prepare as prep
from src.features.build_features import add_y_pred_to_data


def isSeizureBefore(isSeizure):
    """ Marks if the timepoint before is a marked seizure too"""
    return isSeizure.shift(fill_value=0)


def isSeizureBehind(isSeizure):
    """ Marks if the timepoint before is a marked seizure too"""
    return isSeizure.shift(-1, fill_value=0)


def getSeizureStarts(isSeizure):
    seiz_bef = isSeizureBefore(isSeizure)
    return pd.Series(isSeizure.loc[(seiz_bef == 0) & (isSeizure == 1)].index.values, name='start')


def getSeizureEnds(isSeizure):
    seiz_beh = isSeizureBehind(isSeizure)
    return pd.Series(isSeizure.loc[(seiz_beh == 0) & (isSeizure == 1)].index.values, name='end')


def calcSeizureLengths(seizures_df):
    return seizures_df.end - seizures_df.start



def _resize_array_of_array(arrays, max_length):
    for i, array in enumerate(arrays):
        arrays[i] = array[:max_length]
    return arrays


def get_mean_std(seizures_df, data_series, start=pd.Timedelta('-0.5s'), length=pd.Timedelta('2s'), rescale=False):
    #s_matrix = getPartsOfSeizures(data_series, seizures_df['start'], length=length)
    seiz_raw_data = get_ictal_data(data_series,
                                   seizures_df['start'] + start,
                                   seizures_df['start'] + length,)
    if rescale:
        seiz_raw_data = standard_scale_data(seiz_raw_data, seizures_df['start'], seizures_df['end'])
    step = seiz_raw_data[0].index[1] - seiz_raw_data[0].index[0]
    one_second =  pd.Timedelta('1s')
    step = step / one_second
    start_second = start / one_second
    length_second = length / one_second
    index_second = np.arange(start_second, length_second, step)
    arrays = seiz_raw_data.apply(lambda s: s.values).values
    min_len = len(arrays[0])
    has_diff_len = False
    for array in arrays:
        arr_len = len(array)
        if arr_len != min_len:
            has_diff_len = True
        if arr_len < min_len:
            min_len = arr_len
    if has_diff_len:
        arrays = _resize_array_of_array(arrays, min_len)
    arr_stack = np.vstack(arrays)
    arr_mean = np.mean(arr_stack, axis=0)
    # sometimes due to floating errors, arr mean and index_second have different size
    arr_mean.resize(index_second.shape)
    arr_std  = np.std(arr_stack, axis=0)
    arr_std.resize(index_second.shape)
    mean_s = pd.Series(arr_mean, index=index_second)
    std_s = pd.Series(arr_std, index=index_second)
    return mean_s, std_s



def standard_scale_data(data_series, fit_starts, fit_ends):
    """
    Rescale series of raw seizure data, with a standardscaler
    :param data_series:
    :param fit_starts:
    :param fit_ends:
    :return:
    """
    for i, (row, start, end) in enumerate(zip(data_series, fit_starts, fit_ends)):
        fit_part = row.loc[start:end]
        data_series[i] = (row - fit_part.mean())/fit_part.std()
    return data_series

def get_ictal_data(data_series, seiz_starts, seiz_ends):
    ictal_data_series = pd.Series([]) # TODO saving in list and using concat should be faster
    for i, (start, end) in enumerate(zip(seiz_starts, seiz_ends)):
        ictal_data_series[i] = data_series.loc[start:end]
    return ictal_data_series

def get_nonictal_data(data_series, seiz_starts, seiz_ends):
    nonictal_data = pd.Series([])
    last_end   = data_series.index[0]
    for i, (start, end) in enumerate(zip(seiz_starts, seiz_ends)):
        nonictal_data[i] = data_series.loc[last_end:start]
        last_end = end
    nonictal_data.append(data_series.loc[last_end:])
    return nonictal_data


"""
def getPartsOfSeizures(data_series, starts, freq=2000, length=pd.to_timedelta('1s'), rescale=False):

    seiz_data = np.empty([len(starts),
                          int(freq * (length / pd.Timedelta('1s')))])
    for i, start in enumerate(starts):
        data = data_series.loc[start:(start + length)].to_numpy()[:-1]  # the last element is too much
        #  print(type(seiz_data[i]), seiz_data[i])
        seiz_data[i] = data


    if rescale:  # TODO use scaler only for seizure time, not before

        seiz_data = scaler.fit_transform(np.transpose(seiz_data))
        seiz_data = np.transpose(seiz_data)
    return seiz_data
"""

# def getFirstSecondOfSeizures(data_series, starts, freq=2000):
#    """ returns numpy nested array! """
#
#    second = pd.to_timedelta('1s')
#    return getPartsOfSeizures(data_series, starts, freq, second)


# todo test
def createSeizuresDF(starts, ends, raw_df=None):
    if len(starts) != len(ends):
        raise ValueError('The amount of seizure starts differs from the amount of ends.')
    seizures_df = pd.concat([starts, ends], axis=1, names=['start', 'end'])
    seizures_df.columns = ['start', 'end']
    seizures_df.index.name = 's_nr'
    seizures_df['length'] = calcSeizureLengths(seizures_df)
    if raw_df is not None:
        seizures_df['data'] = get_ictal_data(raw_df['yvalue'], starts, ends)
    return seizures_df


def create_seizures_df_from_series(isSeizure, raw_df=None):
    """ if raw, will add 'data' column for seizure """
    starts = getSeizureStarts(isSeizure)
    ends = getSeizureEnds(isSeizure)

    """
    if unequal exceptioin:
        calc length
        add raw data
        """
    return createSeizuresDF(starts, ends, raw_df)


def markSeizures(df, startend_df):
    isSeizure = pd.Series(index=df.index)
    isSeizure[:] = 0
    for index, row in startend_df.iterrows():
        isSeizure.loc[row['start']:row['end']] = 1
    return isSeizure


def mergeSeizures(df):
    """
    Merge seizures with a short break below 1 sec between
    
    Asserts an ordered seizure dataframe
    """
    last_index = df.index[0]
    last_start = df.iloc[0]['start']
    last_end = df.iloc[0]['end']
    new_df = pd.DataFrame(data=None, columns=['start', 'end'])
    for index, row in df[1:].iterrows():
        if row['start'] - last_end < pd.to_timedelta('1s'):
            last_end = row['end']
        else:
            new_df.loc[last_index] = [last_start, last_end]
            last_start = row['start']
            last_end = row['end']
            last_index = index
    new_df['length'] = new_df.end - new_df.start
    new_df = new_df.reset_index()

    return new_df


def adjustTimeDeltaToSeizureStart(seizure_data_series, starts):
    for i, row in enumerate(seizure_data_series):
        row.index = (row.index.values - starts[i])/pd.Timedelta('1s')  # nanoseconds to seconds
    return seizure_data_series


def loadSeizures(filepath):
    "Has to be saved as float/int times 2000 equals start and end points in seconds"
    seizures_df = prep.load_csv(filepath,
                                sep="\t", header=None, names=['start', 'end']);
    seizures_df.start = prep.convertFloatToTimeDelta(seizures_df.start)
    seizures_df.end = prep.convertFloatToTimeDelta(seizures_df.end)
    return createSeizuresDF(seizures_df['start'], seizures_df['end'])


def saveSeizures(seizures_df, filepath):
    "Has to be saved as float/int times 2000 equals start and end points in seconds"
    float_seizures_df = pd.DataFrame()
    float_seizures_df['start'] = (prep.convertTimeDeltaToFloat(seizures_df['start']))
    float_seizures_df['end'] = (prep.convertTimeDeltaToFloat(seizures_df['end']))
    float_seizures_df.to_csv(filepath, index=False, header=False, sep='\t')  # TODO use generice function
