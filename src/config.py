mainconfig = {
    "project": {
        "name": "machine-learning",
        "dir": "~/Projects/machine-learning/",
        "data_dir": "~/Projects/Data/",
    },
    "calcium_data": {
        "trace_properties_file_path": "~/Projects/Data/raw/calcium/trace_properties.csv",
        "raw_files_dir": "~/Projects/Data/raw/calcium/",
        "filtered_files_dir": "~/Projects/Data/interim/filtered/",
    },
    "variance_algorithm": {
        "thresholdsfilepath": "~/Projects/Data/algorithms/variance_algorithm/thresholds.dat",
        "savedirpath": "~/Projects/Data/algorithms/variance_algorithm/seizures/"
    },
    "peak_detect_algorithm": {
        "savedirpath": "~/Projects/Data/algorithms/peak_detect_algorithm/seizures/"
    },
    "decision_tree_algorithm": {
        "savedirpath": "~/Projects/Data/algorithms/decision_tree_algorithm/seizures/",
        "clf_save_file": "~/Projects/Data/algorithms/decision_tree_algorithm/clf_fit.joblib",
    },
    "random_forest_algorithm": {
        "savedirpath": "~/Projects/Data/algorithms/random_forest_algorithm/seizures/",
    },
    "k_neighbors_algorithm": {
        "savedirpath": "~/Projects/Data/algorithms/k_neighbors_algorithm/seizures/",
    },
    "support_vector_algorithm": {
        "savedirpath": "~/Projects/Data/algorithms/support_vector_algorithm/seizures/",
    }
}
