from src.algorithms import\
    variance_algorithm as v_algo,\
    peak_detect_algorithm as pd_algo,\
    decision_tree_algorithm as dt_algo,\
    random_forest_algorithm as rf_algo,\
    k_neighbors_algorithm as kn_algo,\
    support_vector_algorithm as sv_algo


import src.data.seizures as seiz

def get_algo_dict():
    algo_dict = {'variance_algorithm': v_algo,
                 'peak_detect_algorithm': pd_algo,
                 'decision_tree_algorithm': dt_algo,
                 'random_forest_algorithm': rf_algo,
                 'k_neighbors_algorithm': kn_algo,
                 'support_vector_algorithm': sv_algo}
    return algo_dict

def select_algorithm(algorithm):
    algo_dict = get_algo_dict()
    return algo_dict[algorithm].algorithm

def save(seizures_df, file_path):
    seiz.saveSeizures(seizures_df, file_path)


def load(file_path):
    return seiz.loadSeizures(file_path)