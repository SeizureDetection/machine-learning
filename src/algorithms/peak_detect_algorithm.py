import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal

import src.algorithms
import src.data.prepare as prep
import src.data.seizures as seiz
import src.algorithms.run as algo_run
from src.visualization.seizures import plotSeizureLengthHist
from src.visualization.visualize import save_figure
from src.config import mainconfig



def algorithm(name, data_s, config=None):
    """peak detect algorithm"""
    plot = algo_run.is_plot_set(config)
    data_sampled = pd.DataFrame(prep.resampleToMean(data_s, '10L'))
    data_sampled['var'] = data_sampled.rolling(4).var()
    data_sampled['time'] = data_sampled.index

    min_height = data_sampled['var'].mean()
    std = data_sampled['var'].std()

    threshold = min_height + std

    peaks, _ = signal.find_peaks(data_sampled['var'],
                                 height=threshold,
                                 # threshold=min_height,
                                 distance=10,
                                 prominence=min_height,
                                 width=1.5
                                 )

    if plot: plotPeaks(peaks,
                    data_sampled,
                    'var',
                    threshold,
                    title=name,
                    #    filename=name + '_allpeaks',
                    nodatetime=True)

    prominences = signal.peak_prominences(data_sampled['var'], peaks)[0] # TODO move into plot function

    # TODO use with width for sns jointplot
    if plot: plotPeakProminencesHist(prominences,
                                     #                          filename=df['filename'] + '_peakprominences',
                                     nodatetime=True)

    results_half_width = signal.peak_widths(data_sampled['var'], peaks, rel_height=0.5)

    if plot: plotPeakProperties(peaks, data_sampled['var'], prominences, results_half_width, threshold)

    if plot: plotPeakWidthHist(results_half_width[0])

    if plot: plotPeakDiff(peaks)
    # diff <= 25 seizure

    pos_seizure = data_sampled.iloc[peaks].copy()

    if plot: plotPossibleSeizurePeaksHist(pos_seizure)

    pos_seizure['peaks'] = peaks # TODO move into groupSeizures function
    pos_seizure['length_to_last_peak'] = pos_seizure['peaks'].diff()
    pos_seizure['pos_start'] = np.where((pos_seizure['length_to_last_peak'] > 50), 1, 0)
    pos_seizure['pos_end'] = np.where((pos_seizure['length_to_last_peak'].shift(-1) > 25), 1, 0)
    cleaned_peaks = pos_seizure
    seizures_df = groupSeizures(cleaned_peaks)

    if plot: plotSeizureLengthHist(seizures_df['length'])

    if plot: plotSeizurePeaks(data_sampled,
                              cleaned_peaks.loc[seizures_df['start']],
                              cleaned_peaks.loc[seizures_df['end']])
    if algo_run.is_save_set(config):
        save(seizures_df, name)
    return (seizures_df,)


def groupSeizures(df): # TODO move to dat seizures and test
    data = []
    last_index = df.index[0]
    pos_start = df.index[0]
    for index, row in df[1:].iterrows():
        if row['length_to_last_peak'] > 50:
            data.append([pos_start, last_index])
            pos_start = index
        last_index = index
        # todo if pos_start-last_end < 1 add to seizure before
    data.append([pos_start, last_index])
    seizures = pd.DataFrame(data, columns=['start', 'end']) # TODO see line below
    seizures['length'] = seizures.end - seizures.start # TODO use createSeizure
    seizures = seizures[seizures['length'] > pd.to_timedelta('1s')]
    seizures = seiz.mergeSeizures(seizures)
    return seizures


def plotPeaks(peaks_pos, df, y, threshold, title='', filename='', **kwargs):  # dirname='', nodatetime=False):
    """*kwargs: forward to saveFigure"""
    fig, ax = plt.subplots()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Variance peaks')
    ax.set_ylabel('calcium level')
    ax = df.plot(y='yvalue', c='r', ax=ax, label='raw data')
    ax2 = ax.twinx()
    ax2.set_ylabel('variance value')
    df.plot(y=y, c='orange', ax=ax2, label='variance')
    #  ax2.axhline(y=min_height)
    ax2.axhline(y=threshold, label='Threshold')
    df.iloc[peaks_pos].plot(style='.', y=y, c='blue', ax=ax2, label='peaks')
    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax.legend(lines + lines2, labels + labels2, loc=0)
    ax2.get_legend().remove()
    if filename:
        save_figure(fig, filename, **kwargs)
    # todo ttile=
    return fig


def plotPeakProminencesHist(prominences, title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peak pomincences distribution')
    ax = plt.hist(prominences, 50)
    plt.xlabel('variance prominence')
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax


# todo rename to plot PeaksHist and merg with other plots?
def plotPossibleSeizurePeaksHist(pos_seizure, title='', filename='', **kwargs):
    fig, ax = plt.subplots()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Distribution of possible seizure peaks')
    ax = pos_seizure.plot(kind='hist', y='var', bins=50, ax=ax)
    ax.get_legend().remove()
    plt.ylabel('frequency')
    plt.xlabel('variance')
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax


# TODO change xlabel to time
def plotPeakProperties(peaks, y, prominences, results_half, threshold, title='', filename='', **kwargs):
    contour_heights = y[peaks] - prominences
    fig = plt.figure()
    plt.plot(y.values)
    plt.plot(peaks, y[peaks], "x")
    plt.vlines(x=peaks, ymin=contour_heights, ymax=y[peaks])
    plt.hlines(*results_half[1:], color="C2")
    plt.axhline(y=threshold)
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peaks with properties')
    if filename:
        save_figure(fig, filename, **kwargs)
    return plt.show()  # todo necessary??


# TODO width to time or timedelta
# TODO sns plot with fit
def plotPeakWidthHist(width, title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Peak width distribution')
    ax = plt.hist(width, 50)
    plt.xlabel('time')
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax


# peaks distance
# TODO to time data type, give dataframe
def plotPeakDiff(peaks, title='', filename='', **kwargs):
    fig = plt.figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Difference between peaks')
    ax = plt.hist(np.diff(peaks), 50, range=(0, 75))
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax


# TODO rename to plot seizures
# TODO https://matplotlib.org/api/collections_api.html#matplotlib.collections.BrokenBarHCollection.span_where
# https://matplotlib.org/gallery/lines_bars_and_markers/span_regions.html#sphx-glr-gallery-lines-bars-and-markers-span-regions-py
def plotSeizurePeaks(raw_df, start_peaks_df, end_peaks_df, title='', filename='', **kwargs):
    ax = raw_df.plot(x='time', y='var', c='orange')
    # ax = cleaned_peaks.plot(x='s',y='var', style='.', ax=ax)
    fig = ax.get_figure()
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Seizures')

    ax = start_peaks_df.plot(y='var', style='x', ax=ax, c='r', label='start')
    ax = end_peaks_df.plot(y='var', style='x', ax=ax, c='g', label='end')
    if filename:
        save_figure(fig, filename, **kwargs)
    return ax


def get_save_file_path(name):
    return mainconfig["peak_detect_algorithm"]["savedirpath"] + name + '.dat'  # TODO use generic function for file ending


def save(seizures_df, name):
    src.algorithms.save(seizures_df, get_save_file_path(name))


def load(name):
    return src.algorithms.load(get_save_file_path(name))