import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import src.algorithms
import src.algorithms.run as a_run
import src.data.prepare as prep
import src.data.seizures as seiz
import src.visualization.visualize
from src.config import mainconfig


def get_thresholds(names=None):
    filename = mainconfig["variance_algorithm"]["thresholdsfilepath"]
    threshold_df = prep.load_csv(filename, names=['name', 'threshold']).set_index('name')
    if names is not None:
        return threshold_df.loc[names]
    return threshold_df


def timedelta_to_nanoseconds(nptimedelta_series):
    "numpy.timedelta64"
    return (nptimedelta_series / pd.Timedelta('1ns'))


def algorithm(name, data_s, config=None):  # TODO use settings dict, plot
    threshold = a_run.get_config_key(config, 'threshold') # TODO autoload threshold else
    if not threshold:
        threshold = get_thresholds(name)['threshold']
    var_series = data_s.rolling(100).var().dropna();  # TODO window width, freq
    over_threshold = np.array(var_series > threshold)
    # to have integer values for fast +- calculations
    indexes = np.array((timedelta_to_nanoseconds(var_series.index)))
    state = 0  # 0 noseizure, 1 possiblestart, 2 possible seizure, 3 seizure
    possibleStartPeakPos = 0
    possibleEndPeakPos = 0
    starts = []
    ends = []
    # if statement order for performance issues
    for index, o_t in np.nditer([indexes, over_threshold]):
        if o_t:
            if state == 1:
                continue
            if state == 3:
                possibleEndPeakPos = index
                continue
            if state == 0:
                state = 1
                possibleStartPeakPos = index
                possibleEndPeakPos = index
                continue
            if state == 2:
                if index - possibleStartPeakPos < 3e8:  # < np.timedelta64(0.5, 's'):#pd.Timedelta('300ms'):
                    state = 3
                    possibleEndPeakPos = index
                    starts.append(possibleStartPeakPos)
                else:
                    possibleStartPeakPos = index
                continue
        else:
            if state == 2:
                if index - possibleEndPeakPos > 5e8:  # pd.Timedelta('500ms'):
                    state = 0
                continue
            if state == 0:
                continue
            if state == 1:
                state = 2
                continue
            if state == 3:
                if index - possibleEndPeakPos > 5e8:  # pd.Timedelta('500ms'):
                    state = 0
                    ends.append(possibleEndPeakPos)
                    # print('End point ', possibleEndPeakPos, 'added')
                continue

    starts = pd.to_timedelta(pd.Series(starts, dtype='float'), unit='ns')
    ends = pd.to_timedelta(pd.Series(ends, dtype='float'), unit='ns')
    if len(starts) - len(ends) == 1:
        # remove start which did not end in time
        starts = starts[:-1]
    seizures_df = seiz.createSeizuresDF(starts, ends)
    if a_run.is_save_set(config):
        save(seizures_df, name)
    return (seizures_df, var_series)


def get_save_file_path(name):
    return mainconfig["variance_algorithm"]["savedirpath"] + name + '.dat'  # TODO use generic function for file ending


def save(seizures_df, name):
    src.algorithms.save(seizures_df, get_save_file_path(name))


def load(name):
    return src.algorithms.load(get_save_file_path(name))


def plot(data_series, seizures_df, variance_series, threshold):
    fig, ax = plt.subplots(2, sharex=True)
    ax[0].plot(data_series.index.values, data_series, c='b')
    ax[0].plot(seizures_df['start'], data_series.loc[seizures_df['start']], 'go')
    ax[0].plot(seizures_df['end'], data_series.loc[seizures_df['end']], 'ro')
    ax[1].plot(variance_series.index.values, variance_series.values, c='r')
    ax[1].axhline(y=threshold)
    ax[1].plot(seizures_df['start'], np.full(seizures_df['start'].shape, threshold), 'go')
    ax[1].plot(seizures_df['end'], np.full(seizures_df['end'].shape, threshold), 'ro')
    ax[1].xaxis.set_major_formatter(src.visualization.visualize.dateformater())
    for index, row in seizures_df.iterrows():
        ax[0].axvspan(row['start'] / pd.Timedelta('1ns'), row['end'] / pd.Timedelta('1ns'), color='y', alpha=0.3)
        ax[1].axvspan(row['start'] / pd.Timedelta('1ns'), row['end'] / pd.Timedelta('1ns'), color='y', alpha=0.3)
