from sklearn.tree import DecisionTreeClassifier
import pickle
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_validate
import numpy as np

from src.config import mainconfig
import src.data.seizures as seiz
import src.features as feat
import src.algorithms
import src.algorithms.run as algo_run


def fit(X_train, y_train, save=False):
    clf = DecisionTreeClassifier()
    param_grid = {
        'max_depth': [None, 3, 4, 5],
        'max_features': np.arange(3, 15)
    }
    # best_params = {'max_depth': 3, 'max_features': 7}
    CV = GridSearchCV(estimator=clf, param_grid=param_grid, cv=5, n_jobs=-1)
    CV.fit(X_train, y_train);
    print(CV.best_params_)
    print(CV.best_score_)
    clf = CV.best_estimator_
    print(cross_validate(clf, X_train, y_train, cv=5, scoring='accuracy'))
    if save:
        pickle.dump(clf, open('tmp.pkl', 'wb'))
    return clf


def load_clf():
    return pickle.load(open('tmp.pkl', 'rb'))


def algorithm(name, data_s, config=None):
    X_test, y_test, data_df = feat.get_X_y_data_df(name)
    clf = load_clf()
    y_pred = clf.predict(X_test)
    diff = data_s.index.values[-1] - data_s.index.values[0]
    data_df['is_seiz_pred'] = feat.build_features.add_y_pred_to_data(data_df, y_pred)
    seizures_df = seiz.create_seizures_df_from_series(data_df['is_seiz_pred'])
    #seizures_df = seiz.create_seizures_df_from_y_pred(data_df, pd.Series(y_pred))
    if algo_run.is_save_set(config):
        save(seizures_df, name)
    return (seizures_df,)


def get_save_file_path(name):
    return mainconfig["decision_tree_algorithm"]["savedirpath"] + name + '.dat'  # TODO use generic function for file ending


def save(seizures_df, name):
    src.algorithms.save(seizures_df, get_save_file_path(name))


def load(name):
    return src.algorithms.load(get_save_file_path(name))

