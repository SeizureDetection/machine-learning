import pandas as pd
from time import process_time

import src.algorithms
from src.data import prepare
from src import metrics


def run(config, data_df=pd.DataFrame()):
    #Maybe better use pipeline syntax from sklear
    algorithm = get_config_key(config, 'run', throw=True)
    filenames = runs_on(config)
    print('Runs on', filenames)

    algo = src.algorithms.select_algorithm(algorithm)

    if is_report_set(config):
        result_df = pd.DataFrame(index=filenames, columns=['algo_seizures_df', 'report_df'])
    else:
        result_df = pd.DataFrame(index=filenames, columns=['algo_seizures_df'])

    for name in filenames:
        print('Run', name)
        if name in data_df.index:
            data_s = data_df[name]
        elif get_data_type(config):
            data_s = prepare.load_data(name, type=get_data_type(config))
        else:
            data_s = prepare.load_data(name, type='filtered')
        start = process_time()
        algo_return_tuple = algo(name, data_s['data']['yvalue'], config)
        runtime = process_time() - start
        print('Run time (s):', runtime)
        algo_seizures_df = algo_return_tuple[0]
        result_df.loc[name]['algo_seizures_df'] = algo_seizures_df
        if is_report_set(config):
            report_df = metrics.report(name, algorithm, algo_seizures_df, config, runtime=runtime)
            result_df.loc[name]['report_df'] = report_df
        #TODO report/validate
    return result_df


def create_config(algorithm_name, run_on='all', **kwargs):
    return dict(run=algorithm_name, run_on=run_on, **kwargs)


def is_in_run_config(run_config_dict, key):
    if run_config_dict is None:
        raise ValueError('run_config_dict is None')
    if key is None:
        raise ValueError('key is None')
    if key in run_config_dict:
        return True
    return False


def get_config_key(run_config_dict, key, not_found_return=False, throw=False):
    if is_in_run_config(run_config_dict, key):
        return run_config_dict[key]
    if throw:
        raise ValueError(key, 'not set in config')
    return not_found_return


def runs_on(run_config_dict):
    """ Returns the files on which the """
    run_on = get_config_key(run_config_dict, 'run_on')
    if not isinstance(run_on, str):
        return run_on
    if run_on == 'all':
        return prepare.listValidatorFiles()
    return [run_on]


def get_data_type(run_config_dict):
    return get_config_key(run_config_dict, 'data_type')


def get_algorithm(run_config_dict):
    return get_config_key(run_config_dict, 'run')


def is_plot_set(run_config_dict):
    return get_config_key(run_config_dict, 'plot')


def is_save_set(run_config_dict):
    return get_config_key(run_config_dict, 'save')

def is_report_set(run_config_dict):
    return get_config_key(run_config_dict, 'report')